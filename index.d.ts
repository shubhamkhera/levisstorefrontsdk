declare var SkavaUtils={
    /**
    *This function will log in the user and provide cookies according to the session id
    *
    * @param username The username string.
    * @param password password according to the corrosponding user
    */
    userLogin(username: string, password: string): string
    ,
    /**
     * This function gives the profile of the logged in user
     * 
     */
    
    userGetProfile():string
    ,
    /**
     * This function provides the information of the item
     * After passing item's barcode
     * 
     * @param Barcode Takes the requested barcode in form of string
     */
    searchProductByBarcode(barcode:string):string
    ,
    /**
     * This function  provides the items added in the cart by the user
     */
    viewKart():string
}
export {SkavaUtils}