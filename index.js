var axios = require('axios');
var FormData = require('form-data');
var logoutData = new FormData();
var fetch=require('axios')

// config constants
const API_DOMAIN = 'https://b2c-levisdev.skavacommerce.com/orchestrationservices/storefront/';
// "https://b2c-levisdev.skavacommerce.com/orchestrationservices/storefront/customers/login?locale=en_US&storeId=448"
// const API_KEY = 'DVapc08KzR7FbeLXnZFbi7qZ8gawzcEz1kMqPkBy';
const API_KEY = '8.9.0';
const STORE_ID = '448';
const LOCALE = 'en_US';
var addFlag=false;
// config variable
var config = {
  method: '',
  url: '',
  // headers: {
  //   'Access-Control-Allow-Origin':'*',
  //   'Content-Type': 'application/json',
  //   'x-api-key': API_KEY
  // },
  headers: {
    'Content-Type': 'application/json',
    'x-api-key': API_KEY
  },
  data : ''
};

// response data
var SkavaResponse = {};
// error data
var SkavaError = {};
function lets(one,two){
  return one;
}
var SkavaUtils = {
  // skava login function
  userLogin: function(username, password) {
      var message = 'Skava-Lib: Loginng in to Skava Cloud DB...';
      console.log(message);
      // config URL
      var APIEndPoint = API_DOMAIN + "customers/login" + "?locale=" + LOCALE + "&storeId=" + STORE_ID;
      console.log(APIEndPoint);
      config.url = APIEndPoint;
      // config method
      config.withCredentials = true;
      config.method = 'post';
      // config data
      var loginData = { "identity": username, "password": password };
      config.data = JSON.stringify(loginData);
      console.log(config);
      // server call
      return this.axiosLoginCall(config);
      // this.loginTest()
      // return message;
  },
  
  // skava get profile function
  userGetProfile: function() {
      var message = 'Skava-Lib: Retrieving user profile from Skava Cloud DB...';
      console.log(message);
      // config URL
      var APIEndPoint = API_DOMAIN + "customers" + "?locale=" + LOCALE + "&storeId=" + STORE_ID;
      console.log(APIEndPoint);
      config.url = APIEndPoint;
      // config method
      config.method = 'get';
      console.log(JSON.stringify(config));
      // server call
      this.axiosCall(config);
      return message;
  },

  // skava barcode search
  searchProductByBarcode: function(barcode) {
      var Newbarcode="5081025202228"
      var message = 'Skava-Lib: Searching Barcode in Skava Cloud DB...';
      console.log(message);
      // config URL
      // var APIEndPoint = API_DOMAIN + "catalogs/search" + "?locale=" + LOCALE + "&storeId=" + STORE_ID
      //                   + "&searchTerm=" + barcode + "&page=1&size=10";
      var APIEndPoint = API_DOMAIN + "catalogs/search" + "?storeId=" + STORE_ID
                        + "&searchTerm=" + Newbarcode + "&page=1&size=10";
      console.log(APIEndPoint);
      config.url = APIEndPoint;
      // add headers
      config.headers['x-version'] = '8.7.0';
      console.log(JSON.stringify(config.headers));
      // config method
      config.method = 'get';
      // server call
      this.axiosCall(config);
      return message;
  },

   // skava add to cart function
 addCart: function() {
  var message = 'Test-Skava-Lib: Retrieving user cart from Skava Cloud DB...';
  console.log(message);
  // config URL
  addFlag=true;
  var APIEndPoint = API_DOMAIN + "carts/USER/items/" + "?locale=" + LOCALE + "&storeId=" + STORE_ID;
  console.log(APIEndPoint);
  config.url = APIEndPoint;
  // delete config.headers["x-api-key"]
  // config method
  var data = JSON.stringify([{"quantity":1,"skus":[{"skuId":"5081025202228","productId":"5081025202228","type":"DEFAULT"}]}]);
  config.data = data;
  config.method = 'post';
  console.log(JSON.stringify(config));
  // server call
  this.axiosCall(config);
  return message;
  },

  // skava view cart function
 viewCart: function() {
  var message = 'Test-Skava-Lib: Retrieving user cart from Skava Cloud DB...';
  console.log(message);
  // config URL
  var APIEndPoint = API_DOMAIN + "carts/USER" + "?locale=" + LOCALE + "&storeId=" + STORE_ID;
  console.log(APIEndPoint);
  config.url = APIEndPoint;
  // config method
  config.method = 'get';
  console.log(JSON.stringify(config));
  // server call
  this.axiosCall(config);
  return message;
  },

  // skava logout function
  userLogout: function() {
      var message = 'Skava-Lib: Loggin out from Skava Cloud DB...';
      console.log(message);
      // config URL
      var APIEndPoint = API_DOMAIN + "customers/logout" + "?locale=" + LOCALE + "&storeId=" + STORE_ID;
      console.log(APIEndPoint);
      config.url = APIEndPoint;
      // config method
      config.method = 'delete';
      // config data
      config.data = logoutData;
      // server call
      this.axiosCall(config);
      return message;
  },

  // server call for subsequent API End Points after initial Login
  axiosCall: function(config){
    // clear response variable
    SkavaResponse = {};
    // clear error variable
    SkavaError = {};
    // call to server
      axios(config)
      .then(function (response) {
        console.log(config)
        if (addFlag==true){
          delete config.data;
          console.log("Deleted")
        }
        console.log(config)
        console.log(JSON.stringify(response.data));
        SkavaResponse = response;
      })
      .catch(function (error) {
        console.log(error);
        SkavaError = error;
      })
  },

  loginTest: function(){
    fetch('https://b2c-levisdev.skavacommerce.com/orchestrationservices/storefront/customers/login?locale=en_US&storeId=448', {
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    body: JSON.stringify({ "identity": "dev@skava.com", "password": "Skava@123"})
  })
  .then(response => response.json())
  .then(data => console.log(data))
  },

  // server call for Skava Login End Point and retreiving session ID
  axiosLoginCall: function(config){
    // clear response variable
    SkavaResponse = {};
    // clear error variable
    SkavaError = {};
    // call to server
      axios(config)
      .then(function (response) {
        // clear unrequired config params
        // delete config.headers['Content-Type'];
        delete config.data;
        console.log(response.headers)
        // let a=response.headers['set-cookie'][0].toString().split('=')[1].toString()
        // let b=a.split(';')[0]
        
        // let c=response.headers['set-cookie'][1].toString().split('=')[1].toString()
        // let d=c.split(';')[0]
        // let b=""
        // let d=""
        // configure headers for subsequent calls
        // console.log(response)
        // console.log(b)
        // console.log(d)
        config.headers['Cookie'] = 'x-sk-signed-user='+ response.headers['x-sk-signed-user'] + "; "
        + 'x-sk-refresh-id='+ response.headers['x-sk-refresh-id'] +  "; "
        + 'x-sk-session-id=' + response.data.sessionId +  "; ";
        console.log("Config headers");
        console.log(config.headers)
        // console.log(JSON.stringify(config.headers));

        SkavaResponse = config.headers['Cookie'];
        return response.headers;
      })
      .catch(function (error) {
        console.log(error);
        SkavaError = error;
      })
  }
}

export { lets,SkavaUtils, SkavaResponse, SkavaError };
// SkavaUtils.userLogin("pete.smith@skava.com","Skava@123");
